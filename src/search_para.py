#!/bin/env python
# coding: utf-8

import argparse
import sys
import logging
import os
import shutil
import numpy as np
import json
import subprocess
import tempfile
from pathlib import Path
from collections import defaultdict
import numpy as np
import pandas as pd
#from Bio import SeqIO
import math

def aasetup():
    # map for standard amino acids
    aaconv = {}
    aaconv['ALA'] = 'A'
    aaconv['ARG'] = 'R'
    aaconv['ASN'] = 'N'
    aaconv['ASP'] = 'D'
    aaconv['CYS'] = 'C'
    aaconv['GLN'] = 'Q'
    aaconv['GLU'] = 'E'
    aaconv['GLY'] = 'G'
    aaconv['HIS'] = 'H'
    aaconv['ILE'] = 'I'
    aaconv['LEU'] = 'L'
    aaconv['LYS'] = 'K'
    aaconv['MET'] = 'M'
    aaconv['PHE'] = 'F'
    aaconv['PRO'] = 'P'
    aaconv['SER'] = 'S'
    aaconv['THR'] = 'T'
    aaconv['TRP'] = 'W'
    aaconv['TYR'] = 'Y'
    aaconv['VAL'] = 'V'
    return aaconv






# use a single temp folder for this whole calculation
TEMPDIR = tempfile.mkdtemp()

#TEMPDIR = os.path.abspath("./tmp")
#if not os.path.isdir(TEMPDIR):
#    os.makedirs(TEMPDIR)



def fprintf(out,format, *args):
    
    out.write(format % args)
    
def printf(format, *args):
    sys.stdout.write(format % args)

#def write_fasta(f,s):
#    with open(f,"w") as oo:
#        fprintf(oo,">%s\n%s\n",s,s)
        



def write_fasta(f,head,s):

    with open(f,"w") as oo:
        fprintf(oo,">%s\n%s\n",head,s)
        



def read_fasta_seq(fasta_file):

    seq={}


    key = ""
    with open(fasta_file, "r") as fin:
        for line in fin:
            data = line.strip()
            if len(line) == 0:
                continue
            if line[0] == ">":
                key = data[-1]
                
                seq[key] = ""
            else:
                seq[key] += data


    return seq




def read_abag_pdb(pdb_file,hl):

    seq={}
    seqres={}

    seq['H']=""
    seq['L']=""
    seq['O']=""


    seqres['H']=[]
    seqres['L']=[]
    seqres['O']=[]

    xyz = {}
    xyz['H']={}
    xyz['L']={}
    xyz['O']={}

    aaconv = aasetup()



    with open(pdb_file, "r") as fin:
        for line in fin:
            if len(line) >= 17:
                if line[0:6] == "ENDMDL":
                    break
                if line[0:4] == "ATOM":
                    aname = line[12:16]
                    ch = line[21]
                    ires = int(line[22:26])

          
                    x = float(line[30:38])
                    y = float(line[38:46])
                    z = float(line[46:54])
                    r = [x,y,z]

                    mych =""
                    if ch == hl[0]:
                        mych = "H"
                    elif ch == hl[1]:
                        mych = "L"
                    else:
                        mych = "O"


                    if ires not in xyz[mych]:
                        xyz[mych][ires]=[]

                    xyz[mych][ires].append(r)

                    if aname == " CA ":
                        rname = line[17:20]
                        a1 = 'X'
                        if rname in aaconv:
                            a1 = aaconv[rname]


                        seq[mych] += a1
                        seqres[mych].append(ires)



    xyzout = {}
    for ch in xyz:
        xyzout[ch]={}
        for i in xyz[ch]:
            xyzout[ch][i]=np.array(xyz[ch][i])


    return seq,seqres, xyzout




def read_abhet_pdb(pdb_file,hl):

    seq={}
    seqres={}

    seq['H']=""
    seq['L']=""
    seq['O']=""


    seqres['H']=[]
    seqres['L']=[]
    seqres['O']=[]

    xyz = {}
    xyz['H']={}
    xyz['L']={}
    xyz['O']={}

    aaconv = aasetup()

    hetname={}


    with open(pdb_file, "r") as fin:
        for line in fin:
            if len(line) >= 17:
                if line[0:6] == "ENDMDL":
                    break
                if line[0:4] == "ATOM":
                    aname = line[12:16]
                    ch = line[21]
                    ires = int(line[22:26])

          
                    x = float(line[30:38])
                    y = float(line[38:46])
                    z = float(line[46:54])
                    r = [x,y,z]

                    mych =""
                    if ch == hl[0]:
                        mych = "H"
                    elif ch == hl[1]:
                        mych = "L"
                    else:
                        mych = "O"


                    if ires not in xyz[mych]:
                        xyz[mych][ires]=[]

                    xyz[mych][ires].append(r)

                    if aname == " CA ":
                        rname = line[17:20]
                        a1 = 'X'
                        if rname in aaconv:
                            a1 = aaconv[rname]


                        seq[mych] += a1
                        seqres[mych].append(ires)



                elif line[0:6] == "HETATM":
                    aname = line[12:16]
                    ch = line[21]
                    ires = int(line[22:26])

          
                    x = float(line[30:38])
                    y = float(line[38:46])
                    z = float(line[46:54])
                    r = [x,y,z]

                    mych = "O"

                    if ires not in xyz[mych]:
                        xyz[mych][ires]=[]


                    xyz[mych][ires].append(r)

                    rname = line[17:20]

                    if rname not in hetname:

                        a1 = 'X'
                        seq[mych] += a1
                        seqres[mych].append(ires)
                        hetname[rname]=1


    xyzout = {}
    for ch in xyz:
        xyzout[ch]={}
        for i in xyz[ch]:
            xyzout[ch][i]=np.array(xyz[ch][i])


    return seq,seqres, xyzout


def FindOverlap(start1, end1, start2,  end2):

    o = max(0, min(end1, end2) - max(start1, start2) + 1)
    return o

def head2range(header):
    w= header.split("_")
    s1,e1=int(w[3]),int(w[4])
    return s1,e1

def calc_max_overlap(header,stored_header):

    s1,e1 = head2range(header)
    #print(s1,e1)

    omax = 0

    for h in stored_header:
        
        s2,e2 = head2range(h)

        #w= header.split("_")
        #s2,e2=int(w[3]),int(w[4])

        o = FindOverlap(s1, e1, s2,  e2)

        if o > omax:
            omax = o

        #print(s2,e2)

    return omax



def blast2seq(qseq,tseq):

    tempdir = tempfile.mkdtemp()


    qaln = ''
    taln = ''
    qfirst = None
    tfirst = None

    fq = os.path.join(tempdir,"qseq.fa")
    if os.path.isfile(fq):
        os.unlink(fq)

    #print("write",fq,qseq)
    write_fasta(fq,"q",qseq)


    ft = os.path.join(tempdir,"tseq.fa")
    if os.path.isfile(ft):
        os.unlink(ft)

    write_fasta(ft,"t",tseq)


    s=" ";
    fout=tempdir + "/blastout"
    cmd = s.join(("bl2seq",
                  '-i', fq, '-j', ft,
                '-e', '0.0001',
                  '-p', 'blastp',">",fout))

    subprocess.check_call(cmd, shell=True)
    with open(fout,"r") as bfile:

        for line in bfile:

            #print(line.strip())
            if 'Query:' in line:
                w = line.split()
                if qfirst is None:
                    qfirst = int(w[1])
                qaln += w[2]
            else:
                if 'Sbjct:' in line:
                    w = line.split()
                    if tfirst is None:
                        tfirst = int(w[1])
                    taln += w[2]

    aln = {}
    iq = -1
    it = -1

    if qfirst is not None:
        iq = qfirst - 2
    if tfirst is not None:
        it = tfirst - 2
    for ia in range(0, len(qaln)):
        if qaln[ia] != '-':
            iq += 1
        if taln[ia] != '-':
            it += 1
            if qaln[ia] != '-':
                aln[iq] = it


    shutil.rmtree(tempdir)
    return aln


def run_blast(qseq, blastdir,dbname, weight, submat, smin):


    align = []
    para_align = []


    #for c in (hl+'O'):
    #    print(c,len(qseq[c]),len(resnum[c]))
    #sys.exit()

    fq = os.path.join(TEMPDIR,"query.fa")
    if os.path.isfile(fq):
        os.unlink(fq)

    write_fasta(fq,"query",qseq)

    if not os.path.isfile(fq):
        raise Exception('failed to create ' + fq)

        
    fblast = os.path.join(TEMPDIR,"blastout.json")
    evalue = "0.001"





    #print("blastp -query %s -db %s -evalue %s -out %s -outfmt %s" %(fq, clean_db, evalue, fblast, '15'))

    #sys.exit()

    os.system("blastp -query %s -db %s/%s -evalue %s -out %s -outfmt %s -num_alignments 1000000 " %(fq, blastdir, dbname, evalue, fblast, '15'))       


    
    queries = json.loads(open(fblast).read())["BlastOutput2"]

    q2t=[]
    domains=[]


    for query in queries:
        query = query["report"]["results"]["search"]
        query_title = "query"
        query_length = str(query["query_len"])
        hits = query["hits"]
        for idx, hit in enumerate(hits):

            #print(idx,hit)
            tkey = str(hit["description"][0]["title"]).split()[0]


            #tkey = str(hit["description"][0]["title"])

            
            best_score = 0.0
            a={}
            a['tkey'] = tkey
            
            for hsp in hit["hsps"]:
                hsp_score = str(hsp["score"])
                #align_len = str(hsp["align_len"])
                #hsp_identity = str(hsp["identity"])
                #hsp_positive = str(hsp["positive"])
                query_from = str(hsp["query_from"])
                query_to = str(hsp["query_to"])
                hit_from = str(hsp["hit_from"])
                hit_to = str(hsp["hit_to"])
                hsp_qseq = str(hsp["qseq"])
                #hsp_midline = str(hsp["midline"])
                hsp_hseq = str(hsp["hseq"])
                #identity_ratio = "%.3f" %(float(hsp_identity)/float(align_len))
                
                
                #print(tkey)

                if float(hsp_score) > best_score:
                    
                    a['tkey'] = tkey
                    a['score'] = hsp_score
                    a['qbeg'] = int(query_from)
                    a['qend'] = int(query_to)
                    a['tbeg'] = int(hit_from)
                    a['tend'] = int(hit_to)
                    a['qaln'] = hsp_qseq 
                    a['taln'] = hsp_hseq 


            
            #print(a)
            orig_score = float(a['score'])
            #print("blast score",orig_score )
            if smin and orig_score < smin:
                continue

            new_score, paln, q2t1  = calc_weighted_score(a['qbeg'],a['qaln'],a['taln'], weight, submat)
            a['newscore']=new_score
            align.append(a)
            para_align.append(paln)
            q2t.append(q2t1)

        #printf("domain %s %s  range %d - %d\n",w[0],w[1],resnum[is1],resnum[ie1])

    return align,para_align,q2t

def calc_weighted_score(qbeg,qaln,taln, w, submat):

    #print(w)
    score = 0.0

    paln=[]

    
    qaln = list(qaln)
    taln = list(taln)
    iq = qbeg - 2

    q2t={}

    for i in range(len(qaln)):
        qaa = qaln[i]

        if qaa != "-":
            iq += 1

       
        if w[iq] == 0.0:
            continue

        q2t[iq]="-"

    iq = qbeg - 2
    for i in range(len(qaln)):

        qaa = qaln[i]
        taa = taln[i]
        if qaa != "-":
            iq += 1

        if taa == "-": 
            continue


        if w[iq] == 0.0:
            continue

        #print(qaa,taa,iq,w[iq])
        if qaa in submat and taa in submat:

            q2t[iq]=taa

            paln.append([qaa,taa])
            s = submat[qaa][taa]
            score += w[iq]*s
            #print(score)

    sq2t=""
    for key, val in  sorted(q2t.items()):
        sq2t += q2t[key]


    return score,paln, sq2t

def read_blosum(f):
    
    mat = {}
    aa = []
    with open(f,"r") as fh:
        for line in fh:
            w = line.strip().split()
            if w and w[0]=="Head":
                aa=w[1:]
            else:
                if len(w) != 21:
                    raise Exception("cant parse " + f)

                aai = w[0]

                mat[aai]={}
                for i in range(0,20):
                    aaj = aa[i]
                    s = int(w[i+1])
                    mat[aai][aaj]=s
    return aa, mat

def read_fasta(mydir):

    files={}

    for chain in "HL":

        files[chain]=[]
        dir1 = mydir / chain

        input_files = list(dir1.glob('*fa'))

        for f in input_files:

            p = mydir / chain / f
            files[chain].append(p)

    return files

def calc_contacts(qr,tr):

    ncont = 0
    for ires in tr:
        ntat = len(tr[ires])
        if ntat ==0:
            continue
        

        ncont += natom_cont(qr,tr[ires])

    return ncont

def natom_cont(qr,tr):
    dcut = 5
    dmin = 45
    d = np.linalg.norm(qr[0]-tr[0])

    ncont = 0
    if d < dmin:
        for i in range(0,len(qr)):
            for j in range(0,len(tr)):
                d = np.linalg.norm(qr[i]-tr[j])
                if d < dcut:
                    ncont += 1

    return ncont

def sname2key(s):
    #print(s)
    #akey = s.split("_")[:-2]         
    akey = s.split("_")[:-1]        
    key = "_".join(akey)
    return key

def count_donors(f):
    dcount={}
    with open(f,"r") as fh:
        for line in fh:
            if line[0]==">":
                w=line[1:].strip().split()
                #COVID19_Bernardes-2020_1_
                key = sname2key(w[0])
                if key not in dcount:
                    dcount[key]=0
                dcount[key] += 1
    return dcount


def annotate_cdrs(qseq,ch):




    ffasta =os.path.join(TEMPDIR,"query.fa")
    fout =os.path.join(TEMPDIR,"cdrs.txt")

    write_fasta(ffasta,"qseq",qseq)

    exe = "src/anarci_cdrs.py"

    acmd = [exe,"--input",ffasta,"--out",fout,"--chain_type",ch]
    cmd = " ".join(acmd)
    
    os.system(cmd)
    if not os.path.isfile(fout):
        raise Exception("anarci failed")


    pseq=""
    with open(fout,"r") as fh:
        for line in fh:
            if line[0] != ">":
                pseq= line.strip()

    return pseq


def mapseq(a):

    smap=[]

    #i= -1
    j= 0
    for c in a:
        if c != "-":
            #i += 1
            smap.append(j)
        j+= 1

    return smap
    
def compute_cdr_gap(qaln, taln,cdrs):

    ngap=0
    qseq= qaln.replace("-","")
    if cdrs[2] in qseq:
        ibeg =qseq.find(cdrs[2])
        iend = ibeg + len(cdrs[2])

        q2a = mapseq(qaln)

        if iend < len(q2a) :
            jbeg = q2a[ibeg]
            jend = q2a[iend]

            for j in range(jbeg,jend+1):
                if qaln[j] == "-" or taln[j] == "-":
                    ngap += 1


            #print(cdrs[2],qaln[jbeg:jend+1],taln[jbeg:jend+1])

    return ngap


def main():
    # parse arguments
    parser = argparse.ArgumentParser(description='Model Ag on unbound template')
    parser.add_argument("-q", dest='seqin',help="query sequence")
    parser.add_argument('-s', dest='blastdir', type=Path, default="seq/blast/binders",
                        help='folder containing fasta')
    parser.add_argument("-t", dest='pdbin', default="pdb/nCoV396_clean.pdb",help="input PDB")
    parser.add_argument("-o", dest='out_folder', required=True,help="output folder")
    parser.add_argument("--ch", dest='hl', default="HL",help="input PDB heavy-light chains")
    parser.add_argument('-b', dest='blosum_mat',default="blosum.txt", help='blosum62 score')
    parser.add_argument('-m', dest='smin',default=100.0,type=float, help='min score')
    parser.add_argument('-M', dest='bmin',default=550.0,type=float, help='min BLAST score')
    parser.add_argument('-g', dest='gmax',default=2,type=int, help='max CDR3 gap')
    parser.add_argument("--het", dest='hetag', action='store_true',help="antigen is a hetero atom")
    parser.add_argument("--donor", dest='donor', action='store_true',help="compute donor histogram")
    parser.add_argument('--clean', action="store_true",help='remove output folder if it exists')
    args = parser.parse_args()


    #seq = read_fasta(args.seqin)
    aa,submat=read_blosum(args.blosum_mat)

    donorcount={}
    if args.donor:
        for ch in "HL":
            lc = ch.lower()
            ff = args.blastdir / Path(lc + "seq.fa")
            if ff.is_file():
                donorcount[ch] = count_donors(ff)

    if args.hetag:

        qseq, resnum, r = read_abhet_pdb(args.pdbin,args.hl)
    else:
        qseq, resnum, r = read_abag_pdb(args.pdbin,args.hl)


    dout = args.out_folder

    if args.clean and os.path.isdir(dout):
        shutil.rmtree(dout)
    

    if not  os.path.isdir(dout):
        os.makedirs(dout)



    if args.seqin and os.path.isfile(args.seqin):
        seqin =  read_fasta_seq(args.seqin)
        print(args.seqin)

        for ch in "HL":
            if ch in qseq and ch in seqin:
                #print(qseq[ch],seqin[ch])
                t2q = blast2seq(qseq[ch],seqin[ch])
                #print(t2q)

                newseq=""
                for iq in range(0,len(qseq[ch])):
                    aa = qseq[ch][iq]

                    if iq in t2q:
                        jq = t2q[iq]
                        if jq >= 0:
                            aa = seqin[ch][jq]

                    newseq += aa
                qseq[ch]=newseq
                print("newseq", newseq)
            else:
                if ch in qseq :
                    print("ch", ch,"not in qseq ")
                if ch in seqin :
                    print("ch", ch,"not in seqin ")





    cdrs={}
    for ch in "HL":

        if ch not  in qseq:
            continue

        cdr1="X"
        cdr2="X"
        cdr3="X"
        cdr_cout = annotate_cdrs(qseq[ch],ch)
        #print(cdr_cout)
        cdr1,cdr2,cdr3= cdr_cout.split("_")
        cdrs[ch]=[cdr1,cdr2,cdr3]


    agch="O"

    weight = {}
    ncont_tot=0

    #print(r[agch])
    for ch in "HL":
        weight[ch]=[]
        #print(ch,qseq[ch])
        for i in range(0,len(qseq[ch])):
            weight[ch].append(0.0)

            ires = resnum[ch][i]
            nqat = len(r[ch][ires])
            if nqat ==0:
                continue

            #print(r[ch][ires])

            ncont = calc_contacts(r[ch][ires],r[agch])
            ncont_tot += ncont
            weight[ch][i]=ncont
            #print(ch,i,ncont,agch,r[ch][ires])
    if ncont_tot == 0:
        ncont_tot=1

    for ch in "HL":
        for i in range(0,len(qseq[ch])):
            weight[ch][i] /= ncont_tot
            if weight[ch][i] > 0.0:
                print(ch,i,weight[ch][i])

                

    for ch in "HL":



        dbname = "heavy"
        if ch == "L":
            dbname = "light"
        
        fbl = args.blastdir / Path(dbname + ".psq")
        fblx = args.blastdir / Path(dbname + ".00.psq")

        if not fbl.is_file() and not fblx.is_file():
            print("Skipping ",ch)
            continue

        dhits={}



        if ch in donorcount:
            for dkey in donorcount[ch]:
                dhits[dkey]=0

        fout = os.path.join(dout,ch + "_hits.tsv")
        with open(fout,"w") as fh:

            fprintf(fh,"%s","Score\tGap\tQpara\tTpara\tTparaGap\tName\tBlastScore\tQbeg\tQend\tTbeg\tTend\tQaln\tTaln\n")



            # run blast against clean_pdb for each Ag ch
            hits, palgn, q2t = run_blast(qseq[ch], args.blastdir,dbname, weight[ch], submat, args.bmin)
            print("got ",len(hits),"hits",ch)

            for ia in range(0,len(hits)):


                a= hits[ia]
                p= palgn[ia]
                tkey=a['tkey']

                dkey = ""
                if args.donor:
                    dkey = sname2key(tkey)
                    dhits[dkey] += 1
                qa=""
                ta=""
                for pair in p:
                    qa += pair[0]
                    ta += pair[1]


                if args.smin and a['newscore'] < args.smin:
                    continue



                cdr_gap = compute_cdr_gap(a["qaln"],a["taln"],cdrs[ch])


                if cdr_gap > args.gmax:
                    continue


                #print("newscore",a['newscore'],type(a["newscore"]))
                #print("gap",cdr_gap,type(cdr_gap))
                #print("tkey",tkey,type(tkey))
                #print("qa",qa)
                #print("ta",ta)
                #print("tkey",qa)

                #print("score",a['score'],type(a["score"]))
                ###fprintf(fh,"%s","Score\tGap\tQpara\tTpara\tName\tBlastScore\tQbeg\tQend\tTbeg\tTend\tQaln\tTaln\n")
                fprintf(fh,"%8.3f\t%d\t%s\t%s\t%s\t%s\t%s\t%d\t%d\t%d\t%d\t%s\t%s\n",
                       a['newscore'],cdr_gap,qa,ta,q2t[ia],a['tkey'],a['score'],a['qbeg'],a['qend'],a['tbeg'],a['tend'],a["qaln"],a["taln"])

        if args.donor:
            donor_fout= os.path.join(dout,ch + "_donors.tsv")
            with open(donor_fout,"w") as dfh:
                fprintf(dfh,"%s","Donor\tHits\tSeqs\t-logRatio\n")
                for dkey in donorcount[ch]:
                    rat = dhits[dkey]/donorcount[ch][dkey]

                    mlog =0
                    if rat > 0.0:
                        mlog = math.log(rat)

                    fprintf(dfh,"%s\t%d\t%d\t%8.5f\n",dkey,dhits[dkey],donorcount[ch][dkey],-mlog)

    shutil.rmtree(TEMPDIR)


if __name__ == '__main__':
    main()
