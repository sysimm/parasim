#!/bin/env python
# coding: utf-8

import argparse
import sys
import logging
import os
import shutil
import numpy as np
import json
import subprocess
import tempfile
from pathlib import Path
from collections import defaultdict
import pandas as pd
from Bio import SeqIO

def aasetup():
    # map for standard amino acids
    aaconv = {}
    aaconv['ALA'] = 'A'
    aaconv['ARG'] = 'R'
    aaconv['ASN'] = 'N'
    aaconv['ASP'] = 'D'
    aaconv['CYS'] = 'C'
    aaconv['GLN'] = 'Q'
    aaconv['GLU'] = 'E'
    aaconv['GLY'] = 'G'
    aaconv['HIS'] = 'H'
    aaconv['ILE'] = 'I'
    aaconv['LEU'] = 'L'
    aaconv['LYS'] = 'K'
    aaconv['MET'] = 'M'
    aaconv['PHE'] = 'F'
    aaconv['PRO'] = 'P'
    aaconv['SER'] = 'S'
    aaconv['THR'] = 'T'
    aaconv['TRP'] = 'W'
    aaconv['TYR'] = 'Y'
    aaconv['VAL'] = 'V'
    return aaconv






# use a single temp folder for this whole calculation
#TEMPDIR = tempfile.mkdtemp()

TEMPDIR = os.path.abspath("./tmp")




def fprintf(out,format, *args):
    
    out.write(format % args)
    
def printf(format, *args):
    sys.stdout.write(format % args)

def write_fasta(f,s):

    with open(f,"w") as oo:
        fprintf(oo,">%s\n%s\n",s,s)
        



def write_fasta(f,head,s):

    with open(f,"w") as oo:
        fprintf(oo,">%s\n%s\n",head,s)
        



def pdb2seq(pdb_file,hl):
    hseq = ''
    lseq = ''
    oseq = ''
    aaconv = aasetup()

    hseqres=[]
    lseqres=[]
    oseqres=[]

    with open(pdb_file, "r") as fin:
        for line in fin:
            if len(line) >= 17:
                if line[0:6] == "ENDMDL":
                    break
                if line[0:4] == "ATOM":
                    aname = line[12:16]
                    if aname == " CA ":
                        rname = line[17:20]
                        a1 = 'X'
                        ires = int(line[22:26])
                        if rname in aaconv:
                            a1 = aaconv[rname]
                        ch = line[21]
                        if ch == hl[0]:
                            hseq += a1
                            hseqres.append(ires)
                        elif ch == hl[1]:
                            lseq += a1
                            lseqres.append(ires)
                        else:
                            oseq += a1
                            oseqres.append(ires)

    seq={}
    seqres={}

    seq['H']=hseq
    seq['L']=lseq
    seq['O']=oseq

    seqres['H']=hseqres
    seqres['L']=lseqres
    seqres['O']=oseqres

    return seq,seqres


def FindOverlap(start1, end1, start2,  end2):

    o = max(0, min(end1, end2) - max(start1, start2) + 1)
    return o

def head2range(header):
    w= header.split("_")
    s1,e1=int(w[3]),int(w[4])
    return s1,e1

def calc_max_overlap(header,stored_header):

    s1,e1 = head2range(header)
    #print(s1,e1)

    omax = 0

    for h in stored_header:
        
        s2,e2 = head2range(h)

        #w= header.split("_")
        #s2,e2=int(w[3]),int(w[4])

        o = FindOverlap(s1, e1, s2,  e2)

        if o > omax:
            omax = o

        #print(s2,e2)

    return omax

def run_blast(fpdb, hl):


    align = []
    qseq, resnum = pdb2seq(fpdb,hl)


    #for c in (hl+'O'):
    #    print(c,len(qseq[c]),len(resnum[c]))
    #sys.exit()

    fq = os.path.join(TEMPDIR,"query.fa")
    if os.path.isfile(fq):
        os.unlink(fq)

    write_fasta(fq,"query",qseq)

    if not os.path.isfile(fq):
        raise Exception('failed to create ' + fq)

        
    fblast = os.path.join(TEMPDIR,"blastout.json")
    evalue = "0.001"
    clean_db = 'blast/scop'

    #print("blastp -query %s -db %s -evalue %s -out %s -outfmt %s" %(fq, clean_db, evalue, fblast, '15'))

    #sys.exit()

    os.system("blastp -query %s -db %s -evalue %s -out %s -outfmt %s" %(fq, clean_db, evalue, fblast, '15'))       


    
    queries = json.loads(open(fblast).read())["BlastOutput2"]


    domains=[]


    for query in queries:
        query = query["report"]["results"]["search"]
        query_title = str(query["query_title"]).split()[0]
        query_length = str(query["query_len"])
        hits = query["hits"]
        for idx, hit in enumerate(hits):


            hit_id = str(hit["description"][0]["title"]).split()[0]


            tkey = str(hit["description"][0]["title"]).split()[0]
            #print(idx,tkey)
            
            best_score = 0.0
            a={}
            a['tkey'] = tkey
            
            for hsp in hit["hsps"]:
                hsp_score = str(hsp["score"])
                #align_len = str(hsp["align_len"])
                #hsp_identity = str(hsp["identity"])
                #hsp_positive = str(hsp["positive"])
                query_from = str(hsp["query_from"])
                query_to = str(hsp["query_to"])
                hit_from = str(hsp["hit_from"])
                hit_to = str(hsp["hit_to"])
                hsp_qseq = str(hsp["qseq"])
                #hsp_midline = str(hsp["midline"])
                hsp_hseq = str(hsp["hseq"])
                #identity_ratio = "%.3f" %(float(hsp_identity)/float(align_len))
                #print(hit_id,hsp_score)
                
                if float(hsp_score) > best_score:
                    
                    a['score'] = hsp_score
                    a['qbeg'] = query_from
                    a['qend'] = query_to
                    a['tbeg'] = hit_from
                    a['tend'] = hit_to
                    a['qaln'] = hsp_qseq 
                    a['taln'] = hsp_hseq 
                    best_score = float(hsp_score)
            align.append(a)

            overlap = calc_max_overlap(a['tkey'],domains)
            if overlap == 0:
                domains.append(a['tkey'])


    for h in domains:
        w = h.split("_")
        s1,e1 = head2range(h)
        is1=s1-1
        ie1=e1-1

        if is1 < 0:
            continue
        if is1 >= len(resnum) :
            continue
        if ie1 < 0:
            continue
        if ie1 >= len(resnum) :
            continue

        printf("domain %s %s  range %d - %d\n",w[0],w[1],resnum[is1],resnum[ie1])

    return align


def seq_from_tsv(file_index, input_file, dataset_name, gene_names, output_file, sequence_column,
                 v_gene_column, seq_id_column):
    """TSV/AIRR sequence parser."""
    if output_file.is_file():
        logging.debug('reusing existing sequence file %s', output_file)
        return

    logging.debug('extracting sequences from %s', input_file)

    airr = pd.read_table(input_file)

    if airr.empty:
        logging.warning('No data found in input file %s', input_file)
        return

    if set([sequence_column, seq_id_column]).difference(airr.columns):
        logging.warning('Unable to find required data columns: %s, %s in input file %s',
                        sequence_column, seq_id_column, input_file)
        return

    if 'sequence_id' in airr.columns:
        # backup original data because we will overwrite it
        airr['author_sequence_id'] = airr['sequence_id']
        if seq_id_column == 'sequence_id':
            seq_id_column = 'author_sequence_id'

    airr['sequence_id'] = f'{dataset_name}_{file_index}_' + airr.index.astype(str)

    # save global sequence ID for later reference
    airr.to_csv(input_file, sep='\t', index=False)

    mask = airr[sequence_column].str.len() < 50
    if v_gene_column in airr.columns:
        # merge separate masks for the allowed genes
        first_gene, *other_genes = gene_names
        gene_mask = airr[v_gene_column].str.contains(first_gene)
        for other_gene in other_genes:
            # allow any of the genes by OR'ing the masks
            gene_mask |= airr[v_gene_column].str.contains(other_gene)

        # invert gene mask to filter out unwanted sequences
        mask |= ~gene_mask
    else:
        logging.info('V gene column %s not found in file %s. Data was not filtered.', v_gene_column, input_file)

    filtered = airr[~mask]
    if filtered.empty:
        logging.warning('no valid sequences found in %s', input_file)
        return

    fasta = '>' + filtered['sequence_id'] + ' ' + filtered[seq_id_column].astype(str) + '\n' + filtered[sequence_column].str.upper()

    # # compute frequency of each unique aa seq and assign to column sequence_column + '_count'
    # # initialize counts to zero to cover unfiltered data
    # airr[sequence_column + '_count'] = 0

    # # de-duplicate sequences
    # uniq_seqs = filtered[sequence_column].drop_duplicates() # preserves index
    # seq_counts = filtered[sequence_column].value_counts() # removes index
    # seq_counts.name = 'count'
    # seq_index = input_file.with_suffix('.idx')

    # # merge indices with global seq IDs and group sizes
    # uniq_stats = pd.DataFrame(uniq_seqs) \
    #                 .join(filtered['sequence_id']) \
    #                 .merge(seq_counts, left_on=sequence_column, right_index=True)
    # uniq_stats.to_csv(seq_index, sep='\t', index=False)

    # # write unique sequences to fasta
    # uniq_data = filtered.loc[uniq_seqs.index]
    # uniq_fastas = '>' + uniq_data['sequence_id'].astype(str) + ' ' + uniq_data[seq_id_column].astype(str) \
    #               + '\n' + uniq_data[sequence_column]

    # if uniq_fastas.size == 0:
    if fasta.empty:
        logging.warning('No sequences extracted from input file %s', input_file)
        return

    with open(output_file, "w") as outfh:
        outfh.write(fasta.str.cat(sep='\n'))
        # outfh.write('\n'.join(uniq_fastas))
        outfh.write('\n')

    # save global sequence ID and unique sequence ID for later reference
    airr.to_csv(input_file, sep='\t', index=False)

    logging.debug('finished %s', input_file)



def extract_sequences(input_files, dataset_name, aa_seq_column, v_gene_column, seq_id_column):
    """Wrapper for TSV/AIRR sequence parser."""
    chain_to_genes = {
        'H': ['IGH'],
        'L': ['IGL', 'IGK'],
        'A': ['TRA'],
        'B': ['TRB'],
    }

    sequence_files = defaultdict(list)
    for chain in input_files:
        file_id = 0
        for input_file in input_files[chain]:
            out_file = input_file.with_suffix('.fa')
            try:
                seq_from_tsv(file_id, input_file, dataset_name, chain_to_genes[chain],
                             out_file, aa_seq_column, v_gene_column, seq_id_column)
            except Exception:
                logging.exception('Unable to read input file %s', input_file)
                continue

            sequence_files[chain].append(out_file)
            file_id += 1

    return sequence_files


def read_seq(seqdir, name,aa_seq_column, v_gene_column, seq_id_column, seqout):


    if not seqout.is_dir():
        os.makedirs(str(seqout))

    if seqdir.is_dir():
        input_files = list(seqdir.glob('**/*.tsv'))
        if not input_files:
            raise Exception('No input files found inside '+str(seqdir))
    else:
        input_files = [seqdir]



    output_files = {}
    for chain in "HL":
        output_files[chain]=[]
        for input_file in input_files:
            if input_file.name.startswith('._'):
                # Mac OS system files, ignore
                continue
            rel_path = input_file.relative_to(seqdir).parent
            target = Path(seqout) / chain / rel_path


            target.mkdir(exist_ok=True, parents=True)

            shutil.copy(input_file, target)
            output_files[chain].append(target / input_file.name)


        indexed_files = extract_sequences(output_files, name,aa_seq_column,
                                          v_gene_column, seq_id_column)


def main():
    # parse arguments
    parser = argparse.ArgumentParser(description='Model Ag on unbound template')
    parser.add_argument('-q', dest='seqin', type=Path, default="../donorclass/work/Handai_AIRR/BCR/covid",
                        help='folder containing AIRR files or single AIRR file')
    parser.add_argument('-o', dest='seqout', type=Path, default="fasta",
                        help='folder for fasta outputs')
    parser.add_argument("-ch", dest='hl', default="HL",help="input PDB heavy-light chains")
    parser.add_argument('--name', default="query", help='short name for the dataset')
    parser.add_argument('--aa-seq-column', default='sequence_aa',
                        help='TSV column containing the full AA sequence')
    parser.add_argument('--v-gene-column', default='v_call',
                        help='TSV column containing the V gene name (optional)')
    parser.add_argument('--seq-id-column', default='sequence_id',
                        help='TSV column containing the sequence identifier')
    args = parser.parse_args()


    seq = read_seq(args.seqin,  args.name,args.aa_seq_column, args.v_gene_column, args.seq_id_column, args.seqout)



if __name__ == '__main__':
    main()
