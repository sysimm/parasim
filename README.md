# parasim

## Dependencies
Required IMB LSF job queuing system for efficiant execution. Not tested outside our local environment.

Requires blast databases to be prepared

## Usage

The following uses an antibody-antigen complex (nCoV396_clean.pdb, which was constructed from PDB entry 7cr5) to search blast DBs located in seq/blast/nonbinders/

```
mkdir Shuhei_out

./src/search_para.py -p -t pdb/nCoV396_clean.pdb -s seq/blast/nonbinders -o Shuhei_out/binder_scores

```

The output files will be split into "H_hits.tsv" and  "L_hits.tsv" in the provided output dir "Shuhei_out/binder_scores"



note that if you have a query sequence that is highly homologous to the sequence in the pdb file, you can pass it in and it will be used instead of the pdb fiel for the blast search.

```
./src/search_para.py -q 1804HL.fa  -t pdb/nCoV396_clean.pdb -s seq/blast/binders -o Shuhei_out/1804_binder_scores

```
Just make sure your sequence headers in the query fasta file end with "H" and "L" so the program can easily distinguish heavy from light chains.


The following uses an anti-PEG antibody to search blast DBs located in seq/blast/ (note the `-het` flag for non-protein Ag)

```
./src/search_para.py -t pdb/6ju0HLI.pdb -het -s ./seq/blast/HIK-M11_mouse_naive_B -o Hao_out/HIK-M11_mouse_naive_B_score.tsv

```


## Making Blast DBs

You should make separate fasta files for heavy and light chains called 'hseq.fa' and 'lseq.fa'. Then follow the commands in `seq/README.txt`:

```
1. prepare lseq.fa and hseq.fa in blast subdir
Then do

makeblastdb -in lseq.fa -title light -dbtype prot -out light
makeblastdb -in hseq.fa -title heavy -dbtype prot -out heavy
```



## Method

The method is to use BLAST to get alignments betwen the heavy and light chains in the PDB file and the antibody sequences.
Then, for a given alignment, we re-score using:

new_score = SUM[ s(i)*w(i) ] 

where s(i) is the similarity of a pair of aligned residues for(given by the BLOSUM62 score matrix) and w(i) is a weight that depends on the number of contacts in the antibody-antigen complex. The weight for a given residue k is

w(k) = n(k)/SUM[ n(k) ]




## Output

The output file contains the following columns (in tsv format)


Score: weighted score
Qpara: Query paratope residues	
Tpara: Hit paratope residue 
Name: Hit name
BlastScore: Original (full length) Blast score
Qbeg: first query residue in full-length alignment
Qend: last query residue in full-length alignment
Tbeg: first hit residue in full-length alignment
Tend: last hit residue in full-length alignment
Qaln: query sequence in full length alignment
Taln: hit sequence in full length alignment

